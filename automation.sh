#!/bin/sh

#automation of: shebang, touch, chmod, vim

if [ "$#" -ne 1 ]; then
	echo "name of the file"
exit 1
fi

script="$1"

touch "$script"

chmod +x "$script"

echo "#!/bin/bash" > "$script"

vim "$script"
